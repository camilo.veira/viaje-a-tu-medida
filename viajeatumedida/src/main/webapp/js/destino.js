function loaddestiny() {

    let request = sendRequest('destino/list', 'GET', '');

    let tableHTML = document.getElementById("content-table");
    tableHTML.innerHTML = "";

    request.onload = function () {

        let data = request.response;

        data.forEach(row => {

         tableHTML.innerHTML += `

            <tr>
                <td> ${row.idDestino}</td>
                <td>${ row.ciudad.nombreCiudad }</td>
                       
             <td>
                  <a href="verDestino.html?id=${row.idDestino}" class="btn btn-sm btn-success">
                       <i class="bi bi-eye"></i>
                  </a>
                  <a href="editarDestino.html?id=${row.idDestino}" class="btn btn-sm btn-warning">
                       <i class="bi bi-pencil"></i>
                  </a>
                 <button class="btn btn-sm btn-danger" data-bs-id="${row.idDestino}" 
                       data-bs-toggle="modal" data-bs-target="#deleteModal">
                       <i class="bi bi-trash"></i>
                 </button>
             </td>
        
            </tr>
         `;


        });

    }
     request.onerror = function () {
        tableHTML.innerHTML = `
        
          <tr>
            <td colspan="6"> Error al cargar los datos </td>
          </tr>
        `;
     }


}


function loaddestinyById( id , readonly ){
    
  let request = sendRequest('destino/list/'+id, 'GET', '');

  let idDestino = document.getElementById("id");
  let ciudad_nombreCiudad = document.getElementById("ciudad_nombreCiudad");
  
  
  request.onload = function(){

      let data = request.response;

      if( readonly ){
          idDestino.innerHTML = data.idDestino;
          ciudad_nombreCiudad.innerHTML = data.ciudad.nombreCiudad;
         
          }else{
          idDestino.value = data.idDestino;
          ciudad_nombreCiudad.value = data.ciudad.nombreCiudad;
                    
      }

  }

  request.onerror = function(){
      alert('Error al leer el destino');
  }

}


function deletedestiny( id ){

  let request = sendRequest('destino/'+id, 'DELETE', '');

  request.onload = function(){
      loaddestiny();
  }

  request.onerror = function(){
      alert('Error al eliminar');
  }


}


function savedestiny(){

  let idCiudad = document.getElementById("idCiudad").value;
  
  let data = {
         'ciudad':{
            'idCiudad': idCiudad
          
         }
                
  }

  let request = sendRequest('destino/', 'POST', data);

  request.onload = function(){
      window.location = 'destino.html';
  }

  request.onerror = function(){
      alert('Error al crear el destino');
  }

}


function editdestiny(){

  let idDestino = document.getElementById("id").value;
  let idCiudad = document.getElementById("idCiudad").value;
  
  let data = {
    'idDestino': idDestino,
    'ciudad':{
      'idCiudad': idCiudad
    
     }
      
  }

  let request = sendRequest('destino/', 'PUT', data);

  request.onload = function(){
      window.location = 'destino.html';
  }

  request.onerror = function(){
      alert('Error al crear el destino');
  }
}
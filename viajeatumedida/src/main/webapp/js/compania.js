function loadCompany() {

    let request = sendRequest('compania/list', 'GET', '');

    let tableHTML = document.getElementById("content-table");
    tableHTML.innerHTML = "";

    request.onload = function () {

        let data = request.response;

        data.forEach(row => {

         tableHTML.innerHTML += `

            <tr>
                <td>${row.idCompania}</td>
                <td> ${row.nombre}</td>
                <td>${row.tipoIdentificacion}</td>
                <td>${row.identificacion}</td>
                <td class="col-right">  ${row.telefono} </td>
                <td class="col-right">  ${row.correo} </td>
                <td class="col-right">  ${row.tipoPersona} </td>
             <td>
                  <a href="verCompania.html?id=${row.idCompania}" class="btn btn-sm btn-success">
                       <i class="bi bi-eye"></i>
                  </a>
                  <a href="editarCompania.html?id=${row.idCompania}" class="btn btn-sm btn-warning">
                       <i class="bi bi-pencil"></i>
                  </a>
                 <button class="btn btn-sm btn-danger" data-bs-id="${row.idCompania}" 
                       data-bs-toggle="modal" data-bs-target="#deleteModal">
                       <i class="bi bi-trash"></i>
                 </button>
             </td>
        
            </tr>
         `;


        });

    }
     request.onerror = function () {
        tableHTML.innerHTML = `
        
          <tr>
            <td colspan="6"> Error al cargar los datos </td>
          </tr>
        `;
     }


}


function loadCompanyById( id , readonly ){
    
  let request = sendRequest('compania/list/'+id, 'GET', '');

  let idCompania = document.getElementById("id");
  let nombre = document.getElementById("nombre");
  let tipoIdentificacion = document.getElementById("tipoIdentificacion");
  let identificacion = document.getElementById("identificacion")
  let telefono = document.getElementById("telefono");
  let correo = document.getElementById("correo");
  let tipoPersona = document.getElementById("tipoPersona");

  request.onload = function(){

      let data = request.response;

      if( readonly ){
          idCompania.innerHTML = data.idCompania;
          nombre.innerHTML = data.nombre;
          tipoIdentificacion.innerHTML = data.tipoIdentificacion;
          identificacion.innerHTML = data.identificacion;
          telefono.innerHTML = data.telefono;
          correo.innerHTML = data.correo;
          tipoPersona.innerHTML = data.tipoPersona;
      }else{
          idCompania.value = data.idCompania;
          nombre.value = data.nombre;
          tipoIdentificacion.value = data.tipoIdentificacion;
          identificacion.value = data.identificacion;
          telefono.value = data.telefono;
          correo.value = data.correo;
          tipoPersona.value = data.tipoPersona;
      }

  }

  request.onerror = function(){
      alert('Error al leer el compañia');
  }

}


function deleteCompany( id ){

  let request = sendRequest('compania/'+id, 'DELETE', '');

  request.onload = function(){
      loadCompany();
  }

  request.onerror = function(){
      alert('Error al eliminar');
  }


}


function saveCompany(){

  let nombre = document.getElementById("nombre").value;
  let tipoIdentificacion = document.getElementById("tipoIdentificacion").value;
  let identificacion = document.getElementById("identificacion").value;
  let telefono = document.getElementById("telefono").value;
  let correo = document.getElementById("correo").value;
  let tipoPersona = document.getElementById("tipoPersona").value;

  let data = {
      'nombre': nombre,
      'tipoIdentificacion': tipoIdentificacion,
      'identificacion': identificacion,
      'telefono': telefono,
      'correo': correo,
      'tipoPersona': tipoPersona
  }

  let request = sendRequest('compania/', 'POST', data);

  request.onload = function(){
      window.location = 'compania.html';
  }

  request.onerror = function(){
      alert('Error al crear el Compañia');
  }

}


function editCompany(){

  let idCompania = document.getElementById("id").value;
  let nombre = document.getElementById("nombre").value;
  let tipoIdentificacion = document.getElementById("tipoIdentificacion").value;
  let identificacion = document.getElementById("identificacion").value;
  let telefono = document.getElementById("telefono").value;
  let correo = document.getElementById("correo").value;
  let tipoPersona = document.getElementById("tipoPersona").value;

  let data = {
    'idCompania': idCompania,
    'nombre': nombre,
    'tipoIdentificacion': tipoIdentificacion,
    'identificacion': identificacion,
    'telefono': telefono,
    'correo': correo,
    'tipoPersona': tipoPersona
}

  let request = sendRequest('compania/', 'PUT', data);

  request.onload = function(){
      window.location = 'compania.html';
  }

  request.onerror = function(){
      alert('Error al crear el Compañia');
  }
}
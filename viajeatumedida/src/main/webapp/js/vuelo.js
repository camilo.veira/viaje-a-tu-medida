function loadFlight() {

    let request = sendRequest('vuelo/list', 'GET', '');

    let tableHTML = document.getElementById("content-table");
    tableHTML.innerHTML = "";

    request.onload = function () {

        let data = request.response;

        data.forEach(row => {

         tableHTML.innerHTML += `

            <tr>
                <td>${row.idVuelo}</td>
                <td>${row.origen.ciudad.nombreCiudad}</td>
                <td>${row.destino.ciudad.nombreCiudad}</td>
                <td>${row.compania.nombre}</td>
                <td> ${row.fechaVuelo}</td>
                <td>${row.horaVuelo}</td>
                <td>${row.valorVuelo}</td>
                
             <td>
                  <a href="verVuelo.html?id=${row.idVuelo}" class="btn btn-sm btn-success">
                       <i class="bi bi-eye"></i>
                  </a>
                  <a href="editarVuelo.html?id=${row.idVuelo}" class="btn btn-sm btn-warning">
                       <i class="bi bi-pencil"></i>
                  </a>
                 <button class="btn btn-sm btn-danger" data-bs-id="${row.idVuelo}" 
                       data-bs-toggle="modal" data-bs-target="#deleteModal">
                       <i class="bi bi-trash"></i>
                 </button>
             </td>
        
            </tr>
         `;


        });

    }
     request.onerror = function () {
        tableHTML.innerHTML = `
        
          <tr>
            <td colspan="6"> Error al cargar los datos </td>
          </tr>
        `;
     }


}


function loadFlightById( id , readonly ){
    
  let request = sendRequest('vuelo/list/'+id, 'GET', '');

  let idVuelo = document.getElementById("id");
  let origen_ciudad_nombreCiudad = document.getElementById("origen_ciudad_nombreCiudad");
  let destino_ciudad_nombreCiudad = document.getElementById("destino_ciudad_nombreCiudad");
  let compania_nombre = document.getElementById("compania_nombre");
  let fechaVuelo = document.getElementById("fechaVuelo");
  let horaVuelo = document.getElementById("horaVuelo");
  let valorVuelo = document.getElementById("valorVuelo");
  

  request.onload = function(){

      let data = request.response;

      if( readonly ){
          idVuelo.innerHTML = data.idVuelo;
          origen_ciudad_nombreCiudad.innerHTML = data.origen.ciudad.nombreCiudad;
          destino_ciudad_nombreCiudad.innerHTML = data.destino.ciudad.nombreCiudad;
          compania_nombre.innerHTML = data.compania.nombre;
          fechaVuelo.innerHTML = data.fechaVuelo;
          horaVuelo.innerHTML = data.horaVuelo;
          valorVuelo.innerHTML = data.valorVuelo;
         
          
      }else{
          idVuelo.value = data.idVuelo;
          origen_ciudad_nombreCiudad.innerHTML = data.ciudad.nombreCiudad;
          destino_ciudad_nombreCiudad.innerHTML = data.ciudad.nombreCiudad;
          compania_nombre.innerHTML = data.compania.nombre;
          fechaVuelo.value = data.fechaVuelo;
          horaVuelo.value = data.horaVuelo;
          valorVuelo.value = data.valorVuelo;
          
      }

  }

  request.onerror = function(){
      alert('Error al leer el vuelo');
  }

}


function deleteFlight( id ){

  let request = sendRequest('vuelo/'+id, 'DELETE', '');

  request.onload = function(){
      loadFlight();
  }

  request.onerror = function(){
      alert('Error al eliminar');
  }


}


function saveFlight(){

  let idOrigen = document.getElementById("idOrigen").value;
  let idDestino = document.getElementById("idDestino").value;
  let idCompania = document.getElementById("idCompania").value;
  let fechaVuelo = document.getElementById("fechaVuelo").value;
  let horaVuelo = document.getElementById("horaVuelo").value;
  let valorVuelo = document.getElementById("valorVuelo").value;
  

  let data = {

      'origen': {
        'idOrigen' : idOrigen
      },
      'destino': {
        'idDestino' : idDestino
      },
      'compania': {
        'idCompania' : idCompania
      },
      'fechaVuelo': fechaVuelo,
      'horaVuelo': horaVuelo,
      'valorVuelo': valorVuelo
      
  }

  let request = sendRequest('vuelo/', 'POST', data);

  request.onload = function(){
      window.location = 'vuelo.html';
  }

  request.onerror = function(){
      alert('Error al crear el Vuelo');
  }

}


function editFlight(){

  let idVuelo = document.getElementById("id").value;
  let idOrigen = document.getElementById("idOrigen").value;
  let idDestino = document.getElementById("idDestino").value;
  let idCompania = document.getElementById("idCompania").value;
  let fechaVuelo = document.getElementById("fechaVuelo").value;
  let horaVuelo = document.getElementById("horaVuelo").value;
  let valorVuelo = document.getElementById("valorVuelo").value;
  

  let data ={
    
    'idVuelo': idVuelo,
    'origen': {
      'idOrigen' : idOrigen
    },
    'destino': {
      'idDestino' : idDestino
    },
    'compania': {
      'idCompania' : idCompania
    },
    'fechaVuelo': fechaVuelo,
    'horaVuelo': horaVuelo,
    'valorVuelo': valorVuelo
    
}

  let request = sendRequest('vuelo/', 'PUT', data);

  request.onload = function(){
      window.location = 'vuelo.html';
  }

  request.onerror = function(){
      alert('Error al crear el vuelo');
  }
}
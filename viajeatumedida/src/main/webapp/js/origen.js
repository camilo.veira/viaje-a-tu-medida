function loadOrigin() {

    let request = sendRequest('origen/list', 'GET', '');

    let tableHTML = document.getElementById("content-table");
    tableHTML.innerHTML = "";

    request.onload = function () {

        let data = request.response;

        data.forEach(row => {

         tableHTML.innerHTML += `

            <tr>
                <td> ${row.idOrigen}</td>
                <td>${ row.ciudad.nombreCiudad }</td>
                       
             <td>
                  <a href="verOrigen.html?id=${row.idOrigen}" class="btn btn-sm btn-success">
                       <i class="bi bi-eye"></i>
                  </a>
                  <a href="editarOrigen.html?id=${row.idOrigen}" class="btn btn-sm btn-warning">
                       <i class="bi bi-pencil"></i>
                  </a>
                 <button class="btn btn-sm btn-danger" data-bs-id="${row.idOrigen}" 
                       data-bs-toggle="modal" data-bs-target="#deleteModal">
                       <i class="bi bi-trash"></i>
                 </button>
             </td>
        
            </tr>
         `;


        });

    }
     request.onerror = function () {
        tableHTML.innerHTML = `
        
          <tr>
            <td colspan="6"> Error al cargar los datos </td>
          </tr>
        `;
     }


}


function loadOriginById( id , readonly ){
    
  let request = sendRequest('origen/list/'+id, 'GET', '');
  

  let idOrigen = document.getElementById("id");
  let ciudad_nombreCiudad = document.getElementById("ciudad_nombreCiudad");
  
  
  request.onload = function(){

      let data = request.response;
      
      if( readonly ){
        //console.log(data.ciudad.departamento);
        idOrigen.innerHTML = data.idOrigen;
        ciudad_nombreCiudad.innerHTML = data.ciudad.nombreCiudad;
        // console.log(ciudad_nombreCiudad);

          }else{
          idOrigen.value = data.idOrigen;
          ciudad_nombreCiudad.value = data.ciudad.nombreCiudad;
          
                    
      }

  console.log(request);
    }
  request.onerror = function(){
      alert('Error al leer el origen');
  }

}


function deleteOrigin( id ){

  let request = sendRequest('origen/'+id, 'DELETE', '');

  request.onload = function(){
      loadOrigin();
  }

  request.onerror = function(){
      alert('Error al eliminar');
  }


}


function saveOrigin(){

  let idCiudad = document.getElementById("idCiudad").value;
  
  let data = {
         'ciudad':{
            'idCiudad': idCiudad
          
         }
                
  }

  let request = sendRequest('origen/', 'POST', data);

  request.onload = function(){
      window.location = 'origen.html';
  }

  request.onerror = function(){
      alert('Error al crear el origen');
  }

}


function editOrigin(){

  let idOrigen = document.getElementById("id").value;
  let idCiudad = document.getElementById("idCiudad").value;
  
  let data = {
    'idOrigen': idOrigen,
    'ciudad':{
      'idCiudad': idCiudad
    
     }
      
  }

  let request = sendRequest('origen/', 'PUT', data);

  request.onload = function(){
      window.location = 'origen.html';
  }

  request.onerror = function(){
      alert('Error al crear el origen');
  }
}
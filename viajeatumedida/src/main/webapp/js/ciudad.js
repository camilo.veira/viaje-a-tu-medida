function loadCity() {

    let request = sendRequest('ciudad/list', 'GET', '');

    let tableHTML = document.getElementById("content-table");
    tableHTML.innerHTML = "";

    request.onload = function () {

        let data = request.response;

        data.forEach(row => {

         tableHTML.innerHTML += `

            <tr>
                <td>${row.idCiudad}</td>
                <td> ${row.nombreCiudad}</td>
                <td>${row.nombreAreopuerto}</td>
                <td>${row.departamento}</td>
                
             <td>
                  <a href="verCiudad.html?id=${row.idCiudad}" class="btn btn-sm btn-success">
                       <i class="bi bi-eye"></i>
                  </a>
                  <a href="editarCiudad.html?id=${row.idCiudad}" class="btn btn-sm btn-warning">
                       <i class="bi bi-pencil"></i>
                  </a>
                 <button class="btn btn-sm btn-danger" data-bs-id="${row.idCiudad}" 
                       data-bs-toggle="modal" data-bs-target="#deleteModal">
                       <i class="bi bi-trash"></i>
                 </button>
             </td>
        
            </tr>
         `;


        });

    }
     request.onerror = function () {
        tableHTML.innerHTML = `
        
          <tr>
            <td colspan="6"> Error al cargar los datos </td>
          </tr>
        `;
     }


}


function loadCityById( id , readonly ){
    
  let request = sendRequest('ciudad/list/'+id, 'GET', '');

  let idCiudad = document.getElementById("id");
  let nombreCiudad = document.getElementById("nombreCiudad");
  let nombreAreopuerto = document.getElementById("nombreAreopuerto");
  let departamento = document.getElementById("departamento");
  
  request.onload = function(){

      let data = request.response;

      if( readonly ){
          idCiudad.innerHTML = data.idCiudad;
          nombreCiudad.innerHTML = data.nombreCiudad;
          nombreAreopuerto.innerHTML = data.nombreAreopuerto;
          departamento.innerHTML = data.departamento;
          }else{
          idCiudad.value = data.idCiudad;
          nombreCiudad.value = data.nombreCiudad;
          nombreAreopuerto.value = data.nombreAreopuerto;
          departamento.value = data.departamento;
          
      }

  }

  request.onerror = function(){
      alert('Error al leer el ciudad');
  }

}


function deleteCity( id ){

  let request = sendRequest('ciudad/'+id, 'DELETE', '');

  request.onload = function(){
      loadCity();
  }

  request.onerror = function(){
      alert('Error al eliminar');
  }


}


function saveCity(){

  let nombreCiudad = document.getElementById("nombreCiudad").value;
  let nombreAreopuerto = document.getElementById("nombreAreopuerto").value;
  let departamento = document.getElementById("departamento").value;

  let data = {
      'nombreCiudad': nombreCiudad,
      'nombreAreopuerto': nombreAreopuerto,
      'departamento': departamento,
    
  }

  let request = sendRequest('ciudad/', 'POST', data);

  request.onload = function(){
      window.location = 'ciudad.html';
  }

  request.onerror = function(){
      alert('Error al crear el ciudad');
  }

}


function editCity(){

  let idCiudad = document.getElementById("id").value;
  let nombreCiudad = document.getElementById("nombreCiudad").value;
  let nombreAreopuerto = document.getElementById("nombreAreopuerto").value;
  let departamento = document.getElementById("departamento").value;

  let data = {
    'idCiudad': idCiudad,
    'nombreCiudad': nombreCiudad,
    'nombreAreopuerto': nombreAreopuerto,
    'departamento': departamento,
  
  }

  let request = sendRequest('ciudad/', 'PUT', data);

  request.onload = function(){
      window.location = 'ciudad.html';
  }

  request.onerror = function(){
      alert('Error al crear el ciudad');
  }
}
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.viajeatumedida.dao;


import com.misiontic.viajeatumedida.models.Origen;
import org.springframework.data.repository.CrudRepository;


/**
 *
 * @author juanc
 */
public interface OrigenDao extends CrudRepository <Origen, Integer> {
    
}

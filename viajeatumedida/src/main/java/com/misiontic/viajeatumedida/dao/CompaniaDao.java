/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.viajeatumedida.dao;


import com.misiontic.viajeatumedida.models.Compania;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 *
 * @author juanc
 */
@Repository
public interface CompaniaDao extends JpaRepository<Compania, Integer>{
    
    public Compania findByNombre(String name);
}

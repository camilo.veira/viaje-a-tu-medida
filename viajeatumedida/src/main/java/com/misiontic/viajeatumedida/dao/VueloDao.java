/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.viajeatumedida.dao;


import com.misiontic.viajeatumedida.models.Vuelo;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


/**
 *
 * @author juanc
 */
@Repository
public interface VueloDao extends JpaRepository<Vuelo, Integer> {
    
    @Query(nativeQuery = true, value = "SELECT * FROM vuelo V, compania C WHERE C.nombre = ?1 and V.compania_idcompania = C.idcompania;")
    public List<Vuelo> findByCompanyName(String nombre);
    
    @Query(nativeQuery = true, value = "SELECT * FROM vuelo V, ciudad C, destino D WHERE C.nombre = ?1 and V.destino_iddestino = D.iddestino and D.ciudad_idciudad = C.idciudad;")
    public List<Vuelo> findByOriginDestino(String ciudadOrigen,String ciudadDestino);
    
}

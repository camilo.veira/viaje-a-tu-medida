/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.viajeatumedida.dao;


import com.misiontic.viajeatumedida.models.Destino;
import org.springframework.data.repository.CrudRepository;


/**
 *
 * @author juanc
 */
public interface DestinoDao extends CrudRepository <Destino, Integer> {
    
}

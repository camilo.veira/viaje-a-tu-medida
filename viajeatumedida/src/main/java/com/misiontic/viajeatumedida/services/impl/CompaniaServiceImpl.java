/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.viajeatumedida.services.impl;


import com.misiontic.viajeatumedida.dao.CompaniaDao;
import com.misiontic.viajeatumedida.models.Compania;
import com.misiontic.viajeatumedida.services.CompaniaService;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author juanc
 */
@Service
public class CompaniaServiceImpl implements CompaniaService {

    @Autowired
    private CompaniaDao companiaDao;

    @Override
    @Transactional(readOnly = false)
    public Compania save(Compania compania) {
        return companiaDao.save(compania);

    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        companiaDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)    
    public Compania findById(Integer id) {
        return companiaDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Compania> findAll() {
        return (List<Compania>) companiaDao.findAll();
    }

    @Override
    //@Transactional(readOnly = true)  
    public Compania findByName(String name) {
        return companiaDao.findByNombre(name);
    }

    
}

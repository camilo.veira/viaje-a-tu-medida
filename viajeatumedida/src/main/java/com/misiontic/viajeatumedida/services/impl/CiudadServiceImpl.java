/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.viajeatumedida.services.impl;


import com.misiontic.viajeatumedida.dao.CiudadDao;
import com.misiontic.viajeatumedida.models.Ciudad;
import com.misiontic.viajeatumedida.services.CiudadService;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author juanc
 */
@Service
public class CiudadServiceImpl implements CiudadService {

    @Autowired
    private CiudadDao ciudadDao;

    @Override
    @Transactional(readOnly = false)
    public Ciudad save(Ciudad ciudad) {
        return ciudadDao.save(ciudad);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        ciudadDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Ciudad findById(Integer id) {
        return ciudadDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Ciudad> findAll() {
        return (List<Ciudad>) ciudadDao.findAll();
    }

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.viajeatumedida.services;

import com.misiontic.viajeatumedida.models.Compania;
import java.util.List;
/**
 *
 * @author juanc
 */
public interface CompaniaService {
    
    public Compania save ( Compania compania );
    public void delete (Integer id);
    
    public Compania findById (Integer id);
    public List<Compania> findAll ();
    
    public Compania findByName( String name );
   
}

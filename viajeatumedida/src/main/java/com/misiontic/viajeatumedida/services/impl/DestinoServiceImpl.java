/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.viajeatumedida.services.impl;


import com.misiontic.viajeatumedida.dao.DestinoDao;
import com.misiontic.viajeatumedida.models.Destino;
import com.misiontic.viajeatumedida.services.DestinoService;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author juanc
 */
@Service
public class DestinoServiceImpl implements DestinoService {

    @Autowired
    private DestinoDao destinoDao;

    @Override
    @Transactional(readOnly = false)
    public Destino save(Destino destino) {
        return destinoDao.save(destino);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        destinoDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Destino findById(Integer id) {
        return destinoDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Destino> findAll() {
        return (List<Destino>) destinoDao.findAll();
    }

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.viajeatumedida.services.impl;


import com.misiontic.viajeatumedida.dao.VueloDao;
import com.misiontic.viajeatumedida.models.Vuelo;
import com.misiontic.viajeatumedida.services.VueloService;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author juanc
 */
@Service
public class VueloServiceImpl implements VueloService {

    @Autowired
    private VueloDao vueloDao;

    @Override
    @Transactional(readOnly = false)
    public Vuelo save(Vuelo vuelo) {
        return vueloDao.save(vuelo);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        vueloDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Vuelo findById(Integer id) {
        return vueloDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Vuelo> findAll() {
        return (List<Vuelo>) vueloDao.findAll();
    }

    @Override
    public List<Vuelo> findByCompany(String nombre) {
        return (List<Vuelo>) vueloDao.findByCompanyName(nombre);
    }

    @Override
    public List<Vuelo> findByOriginDestino(String ciudadOrigen, String ciudadDestino) {
        return (List<Vuelo>) vueloDao.findByOriginDestino(ciudadOrigen, ciudadDestino);
    }

}

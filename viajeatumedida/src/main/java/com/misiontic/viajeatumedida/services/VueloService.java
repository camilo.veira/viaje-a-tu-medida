/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.viajeatumedida.services;

import com.misiontic.viajeatumedida.models.Vuelo;
import java.util.List;
/**
 *
 * @author juanc
 */
public interface VueloService {
    
    public Vuelo save ( Vuelo vuelo );
    public void delete (Integer id);
    
    public Vuelo findById (Integer id);
    public List<Vuelo> findAll ();
    
    public List<Vuelo> findByCompany(String nombre);
    public List<Vuelo> findByOriginDestino(String ciudadOrigen,String ciudadDestino);
   
}

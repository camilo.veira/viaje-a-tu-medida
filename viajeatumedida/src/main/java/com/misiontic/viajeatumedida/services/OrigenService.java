/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.viajeatumedida.services;
import com.misiontic.viajeatumedida.models.Origen;
import java.util.List;
/**
 *
 * @author juanc
 */
public interface OrigenService {
    
    public Origen save ( Origen origen );
    public void delete (Integer id);
    
    public Origen findById (Integer id);
    public List<Origen> findAll ();
    
    // Se necesita un metodo para filtar por companias
    
}

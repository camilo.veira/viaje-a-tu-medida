/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.viajeatumedida.services;

import com.misiontic.viajeatumedida.models.Destino;
import java.util.List;
/**
 *
 * @author juanc
 */
public interface DestinoService {
    
    public Destino save ( Destino destino );
    public void delete (Integer id);
    
    public Destino findById (Integer id);
    public List<Destino> findAll ();
    
    // Se necesita un metodo para filtar por companias
    
}

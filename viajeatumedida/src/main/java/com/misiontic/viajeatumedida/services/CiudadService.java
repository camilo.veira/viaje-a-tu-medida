/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.viajeatumedida.services;

import com.misiontic.viajeatumedida.models.Ciudad;
import java.util.List;
/**
 *
 * @author juanc
 */
public interface CiudadService {
    
    public Ciudad save ( Ciudad ciudad );
    public void delete (Integer id);
    
    public Ciudad findById (Integer id);
    public List<Ciudad> findAll ();
    
    // Se necesita un metodo para filtar por companias
    
}

package com.misiontic.viajeatumedida;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ViajeatumedidaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ViajeatumedidaApplication.class, args);
	}

}

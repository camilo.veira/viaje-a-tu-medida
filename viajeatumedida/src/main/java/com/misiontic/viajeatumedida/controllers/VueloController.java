/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.viajeatumedida.controllers;

import com.misiontic.viajeatumedida.models.Vuelo;
import com.misiontic.viajeatumedida.services.VueloService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SSmcausil
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/vuelo")
public class VueloController {
    
    @Autowired
    private VueloService vueloService;
    
    
    @GetMapping(value = "/list")
    public List<Vuelo> listarCompania() {
        return vueloService.findAll();
    }

    @GetMapping(value = "/list/{id}")
    public Vuelo consultarPorId(@PathVariable Integer id) {
        return vueloService.findById(id);
    }

    @PostMapping(value = "/")
    public ResponseEntity<Vuelo> agregar(@RequestBody Vuelo vuelo) {
        Vuelo result = vueloService.save(vuelo);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<Vuelo> editar(@RequestBody Vuelo vuelo) {

        Vuelo obj = vueloService.findById(vuelo.getIdVuelo());
            if (obj != null){
            obj.setOrigen(vuelo.getOrigen());
            obj.setDestino(vuelo.getDestino());
            obj.setCompania(vuelo.getCompania());
            obj.setFechaVuelo(vuelo.getFechaVuelo());
            obj.setHoraVuelo(vuelo.getHoraVuelo());
            obj.setValorVuelo(vuelo.getValorVuelo());
            vueloService.save(obj);
            }else{
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Vuelo> eliminar(@PathVariable Integer id) {

        Vuelo result = vueloService.findById(id);

        if (result == null) {
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            vueloService.delete(id);
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
    }
    
    @PostMapping(value = "/list/nombre/{nombre}")
    public List<Vuelo> consultarPorCompania(@PathVariable String nombre) {
        return vueloService.findByCompany(nombre);
    }
    
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.viajeatumedida.controllers;

import com.misiontic.viajeatumedida.models.Destino;
import com.misiontic.viajeatumedida.services.DestinoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SSmcausil
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/destino")
public class DestinoController {
    @Autowired
    private DestinoService destinoService;
    
    @GetMapping(value = "/list")
    public List<Destino> listarDestino() {
        return destinoService.findAll();
    }

    @GetMapping(value = "/list/{id}")
    public Destino consultarPorId(@PathVariable Integer id) {
        return destinoService.findById(id);
    }

    @PostMapping(value = "/")
    public ResponseEntity<Destino> agregar(@RequestBody Destino destino) {
        Destino result = destinoService.save(destino);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
    
    @PutMapping(value="/")
    public ResponseEntity<Destino> editar(@RequestBody Destino destino) {
        Destino obj = destinoService.findById(destino.getIdDestino());
        if(obj != null){
            obj.setCiudad(destino.getCiudad());
            destinoService.save(obj);
        }else{
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Destino> eliminar(@PathVariable Integer id) {

        Destino result = destinoService.findById(id);

        if (result == null) {
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            destinoService.delete(id);
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
    }

    
}

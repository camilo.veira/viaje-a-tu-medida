/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.viajeatumedida.controllers;

import com.misiontic.viajeatumedida.models.Origen;
import com.misiontic.viajeatumedida.services.OrigenService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SSmcausil
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/origen")
public class OrigenController {
    @Autowired
    private OrigenService origenService;
    
    @GetMapping(value = "/list")
    public List<Origen> listarDestino() {
        return origenService.findAll();
    }

    @GetMapping(value = "/list/{id}")
    public Origen consultarPorId(@PathVariable Integer id) {
        return origenService.findById(id);
    }

    @PostMapping(value = "/")
    public ResponseEntity<Origen> agregar(@RequestBody Origen origen) {
        Origen result = origenService.save(origen);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
    
    @PutMapping(value="/")
    public ResponseEntity<Origen> editar(@RequestBody Origen origen) {
        Origen obj = origenService.findById(origen.getIdOrigen());
        if(obj != null){
            obj.setCiudad(origen.getCiudad());
            origenService.save(obj);
        }else{
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Origen> eliminar(@PathVariable Integer id) {

        Origen result = origenService.findById(id);

        if (result == null) {
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            origenService.delete(id);
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
    }
}

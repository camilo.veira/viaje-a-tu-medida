/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.viajeatumedida.controllers;

/**
 *
 * @author SSmcausil
 */
import com.misiontic.viajeatumedida.models.Compania;
import com.misiontic.viajeatumedida.services.CompaniaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/compania")
public class CompaniaController {
    @Autowired
    private CompaniaService companiaService;

    @GetMapping(value = "/list")
    public List<Compania> listarCompania() {
        return companiaService.findAll();
    }

    @GetMapping(value = "/list/{id}")
    public Compania consultarPorId(@PathVariable Integer id) {
        return companiaService.findById(id);
    }
    
    @GetMapping(value = "/list/name/{name}")
    public Compania ConsultarNombreCompania(@PathVariable String name) {
        return companiaService.findByName(name);
    }
    
    @PostMapping(value = "/")
    public ResponseEntity<Compania> agregar(@RequestBody Compania compania) {
        Compania result = companiaService.save(compania);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<Compania> editar(@RequestBody Compania nuevo) {

        Compania actual = companiaService.findById(nuevo.getIdCompania());

        if (actual == null) {
            return new ResponseEntity<>(actual, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            actual.setNombre( nuevo.getNombre());
            actual.setTipoIdentificacion( nuevo.getTipoIdentificacion() );
            actual.setIdentificacion( nuevo.getIdentificacion() );
            actual.setTelefono( nuevo.getTelefono() );
            actual.setCorreo( nuevo.getCorreo() );
            actual.setTipoPersona( nuevo.getTipoPersona() );

            companiaService.save(actual);
            return new ResponseEntity<>(actual, HttpStatus.OK);
        }

    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Compania> eliminar(@PathVariable Integer id) {

        Compania result = companiaService.findById(id);

        if (result == null) {
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            companiaService.delete(id);
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
    }
}

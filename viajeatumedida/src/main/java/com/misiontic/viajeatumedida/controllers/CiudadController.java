/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.viajeatumedida.controllers;

import com.misiontic.viajeatumedida.models.Ciudad;
import com.misiontic.viajeatumedida.services.CiudadService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SSmcausil
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/ciudad")
public class CiudadController {
     @Autowired
    private CiudadService ciudadService;

    @GetMapping(value = "/list")
    public List<Ciudad> listarCiudad() {
        return ciudadService.findAll();
    }

    @GetMapping(value = "/list/{id}")
    public Ciudad consultarPorId(@PathVariable Integer id) {
        return ciudadService.findById(id);
    }

    @PostMapping(value = "/")
    public ResponseEntity<Ciudad> agregar(@RequestBody Ciudad ciudad) {
        Ciudad result = ciudadService.save(ciudad);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<Ciudad> editar(@RequestBody Ciudad nuevo) {

        Ciudad actual = ciudadService.findById(nuevo.getIdCiudad());

        if (actual == null) {
            return new ResponseEntity<>(actual, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            actual.setNombreCiudad( nuevo.getNombreCiudad());
            actual.setNombreAreopuerto( nuevo.getNombreAreopuerto() );
            actual.setDepartamento( nuevo.getDepartamento() );
            ciudadService.save(actual);
            return new ResponseEntity<>(actual, HttpStatus.OK);
        }

    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Ciudad> eliminar(@PathVariable Integer id) {

        Ciudad result = ciudadService.findById(id);

        if (result == null) {
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            ciudadService.delete(id);
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
    }
}

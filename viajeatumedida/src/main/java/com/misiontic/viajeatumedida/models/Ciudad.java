/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.viajeatumedida.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author juanc
 */

@Entity
@Table (name = "ciudad")
@Getter
@Setter
public class Ciudad implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idCiudad")
    private Integer idCiudad;
    
    @Column(name = "nombreCiudad")
    private String nombreCiudad;
    
    @Column(name = "nombreAreopuerto")
    private String nombreAreopuerto;
    
    @Column(name = "departamento")
    private String departamento;
}

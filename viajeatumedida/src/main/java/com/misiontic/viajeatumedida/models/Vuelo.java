/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.viajeatumedida.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;


/**
 *
 * @author juanc
 */

@Entity
@Table (name = "vuelo")
@Getter
@Setter
public class Vuelo implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idVuelo")
    private Integer idVuelo;
    
    @Column(name = "fechaVuelo")
    private String fechaVuelo;
    
    @Column(name = "horaVuelo")
    private String horaVuelo;
    
    @Column(name = "valorVuelo")
    private Double valorVuelo;
    
    @ManyToOne
    @JoinColumn( name="origen_idOrigen")
    private Origen origen;
    
    @ManyToOne
    @JoinColumn( name="destino_idDestino")
    private Destino destino;

    @ManyToOne
    @JoinColumn( name="compania_idCompania")
    private Compania compania;    
    }

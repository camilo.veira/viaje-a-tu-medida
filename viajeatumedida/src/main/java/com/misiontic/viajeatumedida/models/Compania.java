/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.viajeatumedida.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author juanc
 */

@Entity
@Table (name = "compania")
@Getter
@Setter
public class Compania implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idCompania")
    private Integer idCompania;
    
    @Column(name = "nombre")
    private String nombre;
    
    @Column(name = "tipoIdentificacion")
    private String tipoIdentificacion;
    
    @Column(name = "identificacion")
    private String identificacion;
    
    @Column(name = "telefono")
    private String telefono;
    
    @Column(name = "correo")
    private String correo;
    
    @Column(name = "tipoPersona")
    private String tipoPersona;
       
}

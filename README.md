//PROYECTO SELECCIONADO: 
Viaje a tu medida (Vuelafacil.com)

//OBJETIVOS DEL PROYECTO: 
Analizar el problema de manera específica a través de la interacción con el cliente para establecer los objetivos y funcionalidades según su importancia.
Definir los requerimientos funcionales y no funcionales para el aplicativo de software a través de una mirada 360 a lo solicitado por el StakeHolder.
Diseñar las interfaces gráficas de usuario que relacione la base de datos necesaria, permitiendo almacenar la información de manera persistente.
Desarrollar una aplicación de software utilizando una metodología ágil y que cumpla con los aspectos técnicos y funcionales del proyecto.
Realizar pruebas unitarias al funcionamiento del software para verificar que este cumpla con los requisitos planteados por el stakeholder.
Implementar una prueba piloto final en campo que permita determinar si el software cumple con los requerimientos del cliente pensando en el mejoramiento continuo de este. 

By: Grupo 01 Desarrollo de software O40

------------------  REQUERIMIENTOS FUNCIONALES -------------------------------------


Código    Descripción Requerimiento         								        Prioridad

RF1       El sistema permitirá filtrar la información por tipo de aerolínea.            ALTA

RF2		 El sistema permitirá que el usuario pueda escoger el origen y destino 
		 para marcar la ruta de viaje y así poder consultar los vuelos disponibles 
         que tiene la compañía.                                                         ALTA

RF3	     El sistema mostrará mediante una tabla la información relacionada con los 
         siguientes campos Código de vuelo, Origen, Destino, Fecha, Hora de ida, 
         Hora embarque, Empresa o Valor.                                                ALTA

RF4      El sistema permitirá que el usuario filtre por fecha de ida y regreso la 
         disponibilidad de este.                                                        BAJA

RF5      El sistema permitirá que el usuario pueda escoger el vuelo a gusto, teniendo 
         presente que se mostrará información como Código de vuelo, Origen, Destino, 
         Fecha, Hora de ida, Hora embarque, Empresa  y Valor del vuelo, seccionados 
         los vuelos tanto en ida como en vuelta.                                        BAJA

RF6      El sistema podrá generar la tarjeta de embarque mostrando el número de vuelo, 
         origen y destino, fecha del vuelo ida y regreso, aerolínea, 
         puerta de embarque, etc.                                                       BAJA

RF7       El sistema permitirá registrar el historial de consultas del usuario 
          generando a su vez un documento de texto de estos movimientos.                 BAJA

RF8       El sistema permitirá agregar, eliminar, actualizar y refrescar las entradas.   MEDIA




